
{% set mountpoint = '/mnt/media' %}
{% set nfsserver = '40.68.42.108' %}
{% set nfsclients = ['40.68.42.140','40.68.42.163','40.68.27.149'] %} 


nfs:
  server:
    exports:
      /mnt/media: {% for client in nfsclients %} {{ client }}(rw,sync,no_subtree_check) {% endfor %}
  mount:
    media:
      mountpoint: {{ mountpoint }}
      location: {{ nfsserver }}:{{ mountpoint }}
      opts: "rw,vers=3,addr={{ nfsserver }}"
      persist: True
      mkmnt: True