rabbitmq:
  version: "3.6.1-1"
  plugin:
    rabbitmq_management:
      - enabled
  policy:
    rabbitmq_policy:
      - name: HA
      - pattern: '.*'
      - definition: '{"ha-mode": "all"}'
  vhost:
    clms:
      - owner: guest
    clws:
      - owner: guest
  user:
    clmsuser:
      - password: clmspass
      - force: True
      - tags: administrator
      - perms:
        - 'clms':
          - '.*'
          - '.*'
          - '.*'
        - 'clws':
          - '.*'
          - '.*'
          - '.*'
    clwsuser:
      - password: clwspass
      - force: True
      - perms:
        - 'clws':
          - '.*'
          - '.*'
          - '.*'

