users:
  vstoicescu:
    fullname: Valentin Stoicescu
    ssh-keys:
          - ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAh9jYslct/s08yiZcsCGd7c6s6eZ1PSzoLh2G8AvaYGTx3W4UY71bMzGY3mxFl103VTtP23n3iRQyyQ4fqiCdCvWDLi60OSsbpg4X79qqPaFHDCNUCVXSwJwJk8Nh9sCFSAuuG8ChMjQxU9R2XFvh36qYvQECzenB6JYv1CMBYgh2nRW6+Cmh3kpXUzcERJdwMbQx2b2RyGnAIJ9jEqtFGeIDM4voed5HLGSGXSir8xoVS6CW4NSG9QC/uMVK6VfhAZb+iU5VD8oPej+09wN75HFBZSf8vz+VMJivOhuwNeB2Uk1sAh6RL/vTdtIL358SAShbGg76DLA/B7kx0O6Huw==
  tcaraciuc:
    fullname: Tudor Caraciuc
    ssh-keys:
          - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDEHQS0diQ6OOTn9WOU8gK3Tc3Yf9mAJgaZhinvvXSw7dB5AGti//poU+G14t5LrPv98R0c/l19NiBborzXbmKhjBFIy0IC6VgGxtqveE/ueuQvidswbP/mmuuoEAreUyqJCkKu11heVCubWA4xoJIxcXf4w3PnqRD5vhk7CgsO4dq+5L8kELaPhdagCE0QVA1oquK8M/RT27U2NK5n5p1JGKglgRp+AiDLczQpEp4/Veneal5BxCfCJBePPgptbrS7kAeRD36wBmyD4+vhCqtOxq4dQCaxkLnBU6VGgLkhF7G8ucgJ4inHiuAS/5W7ZoJT9KvQdAD3znrnbLi26GDJ
  atij:
    fullname: Andrei Tij
    ssh-keys:
          - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDPpmPGwi9oWZ1mNRpEUWbHoHFErmXuAefEN4T1AcrnCBsZt01oIcWgTtIEVHsFdO54tTDKRWsSZ2BSH5iSsmxR1OUqEOiKZXBzJPz8kirx5lkCuyJzAAATB1hr/rd5D6nS7d2j81rCl2lO+UZL1U4QZa1IhqZnrsQApSQietxQfvFfCQGMgLQwZrA2e1U+hkrn90K4VAQ2zvcERUfWo3FNqcG0OgoEoSIUkE4n8s4jGCcBaiztfQOBj20ByuT/bTCwBvDh/gWIPsryTbB6goTlFS3fXo56aUWo1w55WpTn7FkQqfuVUJGUT7nHf/pZOTspShfCm3Obzs1+anA26Y5P andrei.tij@emag.ro
