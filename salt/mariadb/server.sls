{%- set role = salt['grains.get']('role') -%}
{% set slave = salt['pillar.get']('mysql:dbslave:server', '') %}
{% set master = salt['pillar.get']('mysql:dbmaster:server', '') %}
mariadb-server:
  pkg:
    - installed
    - order: 1
  service:
    - name: mysql
    - running
    - watch:
      - pkg: mariadb-server
      - file: /etc/mysql/my.cnf
    - require:
        - pkg: mariadb-server

{% if role == 'clms' %}
/etc/mysql/my.cnf:
  file:
    - managed
    - source: salt://files/mariadb/clms-my.cnf
    - template: jinja 
    - user: root
    - group: root
    - mode: 644
{% elif role == 'clws' %}
/etc/mysql/my.cnf:
  file:
    - managed
    - source: salt://files/mariadb/my.cnf
    - template: jinja 
    - user: root
    - group: root
    - mode: 644

{% endif %}

#replication 
{% if role == 'clms' %}
{% if grains['db'] == 'master' %}
repluser:
  mysql_user:
    - present
    - host: {{ slave }}
    - password: replpass
    - require:
        - service: mysql
replgrant:
  mysql_grants:
    - present
    - grant: REPLICATION SLAVE
    - database: '*.*'
    - user: repluser
    - host: {{ slave }}
    - require:
        - service: mysql

{% elif grains['db'] == 'slave' %}
startreplica:
  cmd.run:
    - name: mysql --execute="CHANGE MASTER TO MASTER_USE_GTID = current_pos , MASTER_HOST='{{ master }}',MASTER_USER='repluser',MASTER_PASSWORD='replpass',MASTER_PORT=3306, MASTER_CONNECT_RETRY=10;start slave;"
    - unless: mysql --execute="show slave status\G" |grep Slave_ |grep -q Yes
    - require:
      - service: mysql
   
{% endif %}
{% endif %}