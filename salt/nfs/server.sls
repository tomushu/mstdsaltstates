{% from "nfs/map.jinja" import nfs with context %}

{% if nfs.pkgs_server %}
nfs-server-deps:
    pkg.installed:
        - pkgs: {{ nfs.pkgs_server|json }}
{% endif %}
/mnt/media:
  file.directory:
    - user: root
    - group: root
    - mode: 755
    - makedirs: True
/etc/exports:
  file.managed:
    - source: salt://files/nfs/exports
    - template: jinja
    - watch_in:
      - service: nfs-service

nfs-service:
  service.running:
    - name: {{ nfs.service_server }}
    - enable: True