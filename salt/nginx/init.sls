{%- set docroot = salt['grains.get']('role') -%}

nginx:
  pkg:
    - installed
    - order: 1
  service:
    - running
    - watch:
      - pkg: nginx
      - file: /etc/nginx/nginx.conf
      - file: /etc/nginx/vhost.d/{{ docroot }}.mstd.conf
    - require:
        - pkg: nginx
  
/etc/nginx/vhost.d/:
  file.directory:
    - user: root
    - group: root
    - mode: 755
    - makedirs: True

/var/www/{{ docroot }}:
  file.directory:
    - user: www-data
    - group: www-data
    - mode: 755
    - makedirs: True
/etc/nginx/nginx.conf:
  file:
    - managed
    - source: salt://files/nginx/nginx.conf
    - user: root
    - group: root
    - mode: 644
/etc/nginx/vhost.d/{{ docroot }}.mstd.conf:
  file:
    - managed
    - source: salt://files/nginx/{{ docroot }}.mstd.conf
    - template: jinja 
    - user: root
    - group: root
    - mode: 644
    - require:
      - file: /etc/nginx/vhost.d/
nginx-confd:
  file.absent:
        - name: /etc/nginx/conf.d
        - require:
              - pkg: nginx
