php-packs:
 pkg.installed:
  - pkgs:
    - php7.0
    - php7.0-cli
    - php7.0-phpdbg
    - php7.0-fpm
    - php7.0-dev
    - php7.0-curl
    - php7.0-gd
    - php7.0-mcrypt
    - php7.0-readline
    - php7.0-recode
    - php7.0-tidy
    - php7.0-xmlrpc
    - php7.0-xsl
    - php7.0-json
    - php7.0-mysql 
    - php7.0-opcache
    - php7.0-bz2
    - php7.0-bcmath
    - php7.0-mbstring
    - php7.0-xml
    - php-redis
    - php-pear
    - php-amqp
    - php-memcached
    - imagemagick
  - order: 1

/etc/php/7.0/fpm/php-fpm.conf:
  file:
    - managed
    - source: salt://files/fpm/clms/php-fpm.conf
    - template: jinja 
    - user: root
    - group: root
    - mode: 644
/etc/php/7.0/fpm/php.ini:
  file:
    - managed
    - source: salt://files/fpm/clms/php.ini
    - user: root
    - group: root
    - mode: 644
/etc/php/7.0/fpm/pool.d/www.conf:
  file:
    - managed
    - source: salt://files/fpm/clms/www.conf
    - user: root
    - group: root
    - mode: 644
php7.0-fpm:
 service:
   - running
   - enable: True
   - restart: True
   - watch:
     - file: /etc/php/7.0/fpm/php-fpm.conf
     - file: /etc/php/7.0/fpm/pool.d/www.conf
     - file: /etc/php/7.0/fpm/php.ini
