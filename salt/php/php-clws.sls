php-packs:
 pkg.installed:
  - pkgs:
    - php5
    - php5-cli
    - php5-common
    - php5-curl
    - php5-gd
    - php5-json
    - php5-mysql
    - php5-readline
    - php5-recode
    - php5-tidy
    - php5-xmlrpc
    - php5-xsl
    - php5-fpm
    - php5-geoip
    - php5-imagick
    - php5-mcrypt
    - php5-memcached
    - php5-redis
    - imagemagick
    - php5-xcache
    - php-pear
    - pkg-config
  - order: 1
rabbitmq-c-down:
   cmd.run:
       - name: cd /tmp ; git clone git://github.com/alanxz/rabbitmq-c.git;cd rabbitmq-c ; git submodule init ; git submodule update
       - unless: grep -q rabbitmq-c /tmp/rabbitmq-c/configure
rabbitmq-install:
   cmd.run:
       - name: cd /tmp/rabbitmq-c/ ;  autoreconf -i && ./configure && make &&  make install && echo "install_ok" > /tmp/rabbitmq-c.txt
       - unless: grep -q install_ok /tmp/rabbitmq-c.txt


amqp:
  pecl.installed
php5-phalcon-sourcedeps:
 pkg.installed:
  - pkgs: 
    - git-core
    - gcc
    - autoconf
    - make
    - php5-dev
  - require_in:
    - phalcon-source
  - order: 1
phalcon-source:
   cmd.run:
       - name: cd /tmp ; git clone git://github.com/phalcon/cphalcon.git ;cd cphalcon/build ; ./install
       - unless: php -m | grep -q phalcon
/etc/php5/fpm/php-fpm.conf:
  file:
    - managed
    - source: salt://files/fpm/clws/php-fpm.conf
    - template: jinja 
    - user: root
    - group: root
    - mode: 644
/etc/php5/fpm/php.ini:
  file:
    - managed
    - source: salt://files/fpm/clws/php.ini
    - user: root
    - group: root
    - mode: 644
/etc/php5/fpm/pool.d/www.conf:
  file:
    - managed
    - source: salt://files/fpm/clws/www.conf
    - user: root
    - group: root
    - mode: 644

/etc/php5/mods-available/phalcon.ini:
  file:
    - managed
    - source: salt://files/fpm/clws/phalcon.ini
    - user: root
    - group: root
    - mode: 644
/etc/php5/mods-available/amqp.ini:
  file:
    - managed
    - source: salt://files/fpm/clws/amqp.ini
    - user: root
    - group: root
    - mode: 644
/etc/php5/cli/conf.d/20-amqp.ini:
   file.symlink:
      - target: /etc/php5/mods-available/amqp.ini
/etc/php5/fpm/conf.d/amqp.ini:
   file.symlink:
      - target: /etc/php5/mods-available/amqp.ini
php5-fpm:
 service:
   - running
   - enable: True
   - restart: True
   - watch:
     - file: /etc/php5/fpm/php-fpm.conf
     - file: /etc/php5/fpm/php.ini
     - file: /etc/php5/fpm/pool.d/www.conf
     - file: /etc/php5/mods-available/phalcon.ini
     - file: /etc/php5/mods-available/amqp.ini

