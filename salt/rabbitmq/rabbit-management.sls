install_rabbit_management:
  cmd.run:
    - name : curl -k -L http://localhost:15672/cli/rabbitmqadmin -o /usr/local/sbin/rabbitmqadmin
    - unless: ls /usr/local/sbin/rabbitmqadmin  2>/dev/null



chmod_rabbit_management:
  file.managed:
  - name: /usr/local/sbin/rabbitmqadmin
  - user: root
  - group: root
  - mode: 755
  - require:
    - cmd : install_rabbit_management