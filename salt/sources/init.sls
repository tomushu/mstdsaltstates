{%- set role = salt['grains.get']('role') -%}
nginx_repo:
  pkgrepo.managed:
    - humanname: Nginx PPA
    - name: deb http://nginx.org/packages/mainline/ubuntu/ trusty nginx
    - file: /etc/apt/sources.list.d/nginx.list
    - keyid: 7BD9BF62
    - key_url: http://nginx.org/keys/nginx_signing.key
    - keyserver: keyserver.ubuntu.com
    - refresh_db: true
    - require_in:
      - pkg: nginx
    - clean_file: True


{% if role == 'clms' %}
import-php7-ppakey:
    cmd.run:
        - name: apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 4F4EA0AAE5267A6C
        - unless: apt-key list | grep -q Ond
php7_repo:
  pkgrepo.managed:
    - ppa: ondrej/php
    - keyid_ppa: True
    - keyid: E5267A6C
    - keyserver: keyserver.ubuntu.com
    - refresh_db: True
import-rabbitmq-key:
    cmd.run:
        - name: curl https://www.rabbitmq.com/rabbitmq-signing-key-public.asc |  apt-key add -
        - unless: apt-key list | grep -q rabbitmq
rabbitmq_repo:
  pkgrepo.managed:
    - humanname: Rabbitmq REPO
    - name: deb http://www.rabbitmq.com/debian/ testing main
    - file: /etc/apt/sources.list.d/rabbitmq.list
    - refresh_db: true
    - clean_file: True
import-mariadb-keys:
    cmd.run:
        - name: apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xcbcb082a1bb943db
        - unless: apt-key list | grep -q mariadb.org
maria_repo:
  pkgrepo.managed:
    - humanname: MariaDB REPO
    - name: deb [arch=amd64,i386] http://ftp.bme.hu/pub/mirrors/mariadb/repo/10.1/ubuntu trusty main
    - file: /etc/apt/sources.list.d/mariadb.list
    - refresh_db: true
    - clean_file: True

{% endif %}

{% if role == 'clws' %}
import-php56-ppakey:
    cmd.run:
        - name: apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 4F4EA0AAE5267A6C
        - unless: apt-key list | grep -q Ond
php56_repo:
  pkgrepo.managed:
    - ppa: ondrej/php5-5.6 
    - keyid_ppa: True
    - keyid: E5267A6C
    - keyserver: keyserver.ubuntu.com
    - refresh_db: True
phalcon_repo:
  pkgrepo.managed:
    - ppa: phalcon/stable
    - keyid: 1E569794
    - keyserver: keyserver.ubuntu.com
    - refresh_db: True
import-mariadb-keys:
    cmd.run:
        - name: apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xcbcb082a1bb943db
        - unless: apt-key list | grep -q mariadb.org
maria_repo:
  pkgrepo.managed:
    - humanname: MariaDB REPO
    - name: deb [arch=amd64,i386] http://ftp.bme.hu/pub/mirrors/mariadb/repo/10.1/ubuntu trusty main
    - file: /etc/apt/sources.list.d/mariadb.list
    - refresh_db: true
    - clean_file: True
import-rabbitmq-key:
    cmd.run:
        - name: curl https://www.rabbitmq.com/rabbitmq-signing-key-public.asc |  apt-key add -
        - unless: apt-key list | grep -q rabbitmq
rabbitmq_repo:
  pkgrepo.managed:
    - humanname: Rabbitmq REPO
    - name: deb http://www.rabbitmq.com/debian/ testing main
    - file: /etc/apt/sources.list.d/rabbitmq.list
    - refresh_db: true
    - clean_file: True
{% endif %}
