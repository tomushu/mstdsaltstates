system_packs:
 pkg.installed:
  - pkgs:
    - mtr-tiny
    - iptraf-ng
    - git
    - strace
    - htop
    - vim 
    - mc
    - nano
    - telnet
    - screen
    - rsync
    - openssh-client
    - cron
    - python-software-properties
    - python-mysqldb
  - order: 1

ntpdate:
 pkg:
  - installed
ntp:
 pkg:
  - installed
  - order: 1
 service:
   - running
   - enable: True
   - restart: True
   - watch:
     - file: /etc/ntp.conf
     - pkg: ntp
/etc/ntp.conf:
  file:
   - managed
   - source: salt://files/ntp/ntp.conf
sysctl-apply:
  cmd.wait:
    - name:  sysctl -p
    - watch:
      - file: /etc/sysctl.conf
/etc/sysctl.conf:
  file:
  - managed
  - source: salt://files/sysctl/sysctl.conf

