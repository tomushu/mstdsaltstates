{%- set role = salt['grains.get']('role') -%}
base:
  '*':
    - system
    - users
    - sources
    - nginx
{% if role == 'clms' %}
    - php.php-clms
    - nfs.client
    - nfs.mount
    - rabbitmq
    - rabbitmq.rabbit-management
    - rabbitmq.config
    - mariadb.server

{% elif role == 'clws' %}
    - php.php-clws
    - nfs.client
    - nfs.mount
    - mariadb.server
{% endif %}
{% if role == 'ccms' %}
    - nfs.server
{% endif %}