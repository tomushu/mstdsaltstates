{% for user, args in pillar.get('users', {}).iteritems() %}
{{user}}:
  user.present:
    - fullname: {{ args['fullname'] }}
    - shell: /bin/bash
    {% if grains['os'] == 'Ubuntu' %}
    - groups:
      - sudo
      - adm
      - dip
      - cdrom
      - plugdev
      - www-data
    {% endif %}

{% if args['ssh-keys'] %}
{{user}}_root_key:
  ssh_auth.present:
    - user: root
    - names:
      {% for key in args['ssh-keys'] %}
      - {{ key }}
      {% endfor %}

{{user}}_key:
  ssh_auth.present:
    - user: {{user}}
    - names:
      {% for key in args['ssh-keys'] %}
      - {{ key }}
      {% endfor %}
{% endif %}
{% endfor %}

/etc/sudoers.d/sudonopasswd:
  file.managed:
    - source: salt://files/sudoers.d/sudonopasswd
    - user: root
    - group: root
    - mode: 440
